ARG PYTHON_VERSION=3.12

FROM python:${PYTHON_VERSION}

RUN pip install --upgrade pip \
    && pip install poetry
